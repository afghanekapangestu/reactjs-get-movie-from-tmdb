import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const ModalExample = props => {
  const { className } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };

  return (
    <div>
      <Button color="info" onClick={toggle}>
        View
      </Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>{props.movie.title}</ModalHeader>
        <ModalBody>
          <h5>Sinopsis</h5>
          <p>{props.movie.overview}</p>
          <h5>Tanggal Rilis</h5>
          <p>{props.movie.release_date}</p>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={function href() {
              window.location.href =
                "https://www.themoviedb.org/movie/" + props.movie.id;
            }}
          >
            Detail
          </Button>
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default ModalExample;
