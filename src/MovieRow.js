import React from "react";
import { Card, CardImg, CardBody, CardTitle, Button, Col } from "reactstrap";

import Modal from "./ModalDetail.js";

class MovieRow extends React.Component {
  viewMovie() {
    console.log("View");
  }
  render() {
    return (
      <Col md="4">
        <Card
          style={{
            marginTop: 20
          }}
        >
          <CardImg
            top
            width="100%"
            src={this.props.movie.poster_src}
            alt="Card image cap"
          />
          <CardBody>
            <CardTitle>
              <h4>{this.props.movie.title}</h4>
            </CardTitle>
            <Modal movie={this.props.movie} />
          </CardBody>
        </Card>
      </Col>
    );
  }
}

export default MovieRow;
