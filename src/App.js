import React, { Component } from "react";
import "./App.css";
import MovieRow from "./MovieRow.js";
import $ from "jquery";
import { Container, Row } from "reactstrap";
import ModalDetail from "./ModalDetail.js";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.performSearch();
  }

  performSearch(link) {
    console.log("Test Searching");
    const url =
      `https://api.themoviedb.org/3/search/movie?api_key=42ce43706c08039e3142d4ceda84f91f&query=` +
      link;
    $.ajax({
      url: url,
      success: res => {
        const results = res.results;
        console.log(results[0]);

        var movieRows = [];
        var movieDetail = [];

        results.forEach(movie => {
          console.log(movie.title);
          movie.poster_src =
            "https://image.tmdb.org/t/p/w185" + movie.poster_path;
          const movies = <MovieRow key={movie.id} movie={movie} />;
          const movies2 = <ModalDetail key={movie.id} movie={movie} />;
          movieRows.push(movies);
          movieDetail.push(movies2);
        });
        this.setState({ rows: movieRows });
      },
      error: err => {
        console.error(err);
      }
    });
  }

  searchChangeHandler(event) {
    console.log(event.target.value);
    const searching = event.target.value;
    this.performSearch(searching);
  }
  render() {
    return (
      <div>
        <table className="title-bar">
          <tbody>
            <tr>
              <td>
                <h1>MovieDB Search</h1>
              </td>
            </tr>
          </tbody>
        </table>

        <input
          style={{
            fontSize: 24,
            display: "block",
            width: "100%",
            paddingTop: 8,
            paddingBottom: 8,
            paddingLeft: 16,
            fontWeight: "bold"
          }}
          onChange={this.searchChangeHandler.bind(this)}
          type="text"
          placeholder="Enter search term"
        />

        <Container
          style={{
            marginTop: 20
          }}
        >
          <Row>{this.state.rows}</Row>
        </Container>
      </div>
    );
  }
}

export default App;
